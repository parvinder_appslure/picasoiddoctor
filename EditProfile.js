import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    Platform,
    TextInput,
    View,
    Image,
    Alert,
    AsyncStorage,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    FlatList
} from 'react-native';
const window = Dimensions.get('window');
const GLOBAL = require('./Global');
import Button from 'react-native-button';
import ImagePicker from 'react-native-image-picker';
const options = {
    title: 'Select Avatar',
    maxWidth : 500,
    maxHeight : 500,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
type Props = {};
import Header from './Header.js';
export default class EditProfile extends Component {
    state = {
        text: '',
        passwordtext :'',
        isSecure : true,
        username: '',
        city:'',
        address:'',
        password: '',
        status :'',
        ipAdd : '',
        name:'',
        description:'',
        loading:'',
        states:'',
        avatarSource:'',phone:'',exp:'',
        image :'',username:'', flag:0,

    };


    static navigationOptions = ({ navigation }) => {
        return {
              header: () => null,
        }
    }



    showLoading() {
        this.setState({loading: true})
    }

    getNewsUpdate(){

        const url = GLOBAL.BASE_URL +  'doctor_profile'
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                doctor_id : GLOBAL.user_id,

            }),
        }).then((response) => response.json())
            .then((responseJson) => {

                console.log(JSON.stringify(responseJson))
                if (responseJson.status == true) {
                     this.setState({name :responseJson.name})
                     this.setState({description :responseJson.email})
                     this.setState({image :responseJson.image})
                     this.setState({phone : responseJson.phone})
                     this.setState({exp: responseJson.doctor_experience +' Years Experience'})
                     GLOBAL.myimage = responseJson.image
                }else {
                    alert('Something went wrong!')
                }
            })
            .catch((error) => {
                console.error(error);
            });

    }


    changeImage=()=>{
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                GLOBAL.myimage = response.uri
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source,
                });
                this.setState({flag: 1})
            }
        });

    }



    _handleStateChange = state => {
        this.getNewsUpdate()

    };


    buttonClickListener = () =>{
        if (this.state.name == ''){
            alert('Please Enter Name')
        } else if (this.state.description == ''){
            alert('Please Enter Email')
        }
        else {
            //   this.showLoading()
            const url = GLOBAL.BASE_URL +  'update_profile_doctor'
            const data = new FormData();
            data.append('user_id', GLOBAL.user_id);
            data.append('name', this.state.name);
            data.append('flag', this.state.flag);
            // you can append anyone.
            data.append('image', {
                uri: GLOBAL.myimage,
                type: 'image/jpeg', // or photo.type
                name: 'image.png'
            });
            fetch(url, {
                method: 'post',
                body: data,
                headers: {
                    'Content-Type': 'multipart/form-data',
                }

            }).then((response) => response.json())
                .then((responseJson) => {

                    if(responseJson.status==true){
                     GLOBAL.myimage = responseJson.image
                     GLOBAL.myname = responseJson.name
                     GLOBAL.myemail = responseJson.email
                    alert('Profile Updated Successfully!')
                    this.props.navigation.goBack();

                    }else{
                        alert('Something went wrong!')
                    }

                });
        }

    }


    componentWillUnmount() {

    }


    hideLoading() {
        this.setState({loading: false})
    }

    componentDidMount(){
        this.props.navigation.addListener('willFocus',this._handleStateChange);
    }


    render() {


        return (
            <View style={styles.container}>
            <Header navigation={this.props.navigation}
            headerName={'EDIT PROFILE'}
            /> 

                <View style={{marginTop:0,width:window.width, flex:1}}>
                    <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

{/*                        <View style = {{flexDirection:'row',width:window.width,marginTop:50}}>

                            <TouchableOpacity style = {{width :40 ,height : 40, zIndex:1 }}
                                              onPress={() => this.props.navigation.goBack()}>
                                <Image style = {{width :30 ,height : 30 ,marginLeft: 10,resizeMode: 'contain'}}
                                       source={require('./back-arrow.png')}/>

                            </TouchableOpacity>

                            <Text style = {{marginLeft:30,fontSize: 20,color:'#800000',fontFamily: 'Konnect-Medium', width:'70%', marginTop:3}}>
                                Edit Profile
                            </Text>
                        </View>
*/}


                        <View style = {{flexDirection:'column',width:window.width,marginTop:40, alignSelf:'center', alignItems:'center'}}>
                            <TouchableOpacity
                                onPress={() => this.changeImage()}>

                                {this.state.avatarSource != '' && (

                                    <Image style = {{width :100 ,height : 100 ,borderRadius:50, borderColor:'#800000', borderWidth:1}}
                                           source={this.state.avatarSource} />
                                )}
                                {this.state.avatarSource == '' && (

                                    <Image style = {{width :100 ,height : 100 ,borderRadius:50, borderColor:'#800000', borderWidth:1}}
                                           source={{uri:this.state.image}}/>
                                )}

                            </TouchableOpacity>

                            <Text style = {{fontSize: 20,color:'grey',marginTop:25, fontFamily:'Konnect-Regular'}}>
                                Edit Profile Pic
                            </Text>
                        </View>


                    <View style={{width:window.width -40,alignSelf:'center', marginTop:40, }}>
                      <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey'}}>FULL NAME</Text>

                        <TextInput
                            style={{ height: 50, borderColor: '#f3f3f4',fontSize:18, borderBottomWidth: 1, marginTop:0 ,marginBottom: 20 ,width:'100%',color:'black',fontFamily:'Konnect-Regular' }}
                            // Adding hint in TextInput using Placeholder option.
                            placeholder="Enter Name"
                            placeholderTextColor = 'grey'
                            maxLength={35}
                            // Making the Under line Transparent.
                            underlineColorAndroid="transparent"
                            value = {this.state.name}
                            onChangeText={(text) => this.setState({name:text})}
                        />

                      <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:10}}>MOBILE NUMBER</Text>

                        <TextInput
                            style={{ height: 50, borderColor: '#f3f3f4',fontSize:18, borderBottomWidth: 1, marginTop:0 ,marginBottom: 20 ,width:'100%',color:'black', fontFamily:'Konnect-Regular' }}
                            placeholder="Mobile Number"
                            placeholderTextColor = 'grey'
                            maxLength={10}
                            editable={false}
                            keyboardType={'numeric'}
                            underlineColorAndroid="transparent"
                            value = {this.state.phone}
                            onChangeText={(text) => this.setState({phone:text})}
                        />

                      <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:10}}>EXPERIENCE</Text>

                        <TextInput
                            style={{ height: 50, borderColor: '#f3f3f4',fontSize:18, borderBottomWidth: 1, marginTop:0 ,marginBottom: 20 ,width:'100%',color:'black', fontFamily:'Konnect-Regular' }}
                            placeholder="Experience"
                            placeholderTextColor = 'grey'
                            maxLength={35}
                            editable={false}
                            underlineColorAndroid="transparent"
                            value = {this.state.exp}
                            onChangeText={(text) => this.setState({exp:text})}
                        />


                      <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:10}}>EMAIL</Text>
                        <TextInput
                            style={{ height: 50, borderColor: '#f3f3f4',fontSize:18, borderBottomWidth: 1, marginTop:0 ,marginBottom: 20 ,width:'100%',color:'black', fontFamily:'Konnect-Regular' }}
                            placeholder="Enter Email"
                            placeholderTextColor = 'grey'
                            editable={false}
                            underlineColorAndroid="transparent"
                            onChangeText={(text) => this.setState({description:text.replace(/\s+/g,'')})}
                            value = {this.state.description}
                        />
                        </View>



                    <TouchableOpacity style={{marginTop:'10%',alignSelf:'center',marginBottom:'10%'}}
                    onPress={() => this.buttonClickListener()}>
                    <View style = {{backgroundColor:'#800000',height:55,borderRadius:27.5,alignSelf:'center',width:300,
                        borderBottomWidth: 0,
                        shadowColor: 'black',
                        shadowOffset: { width: 0, height: 2 },
                        shadowOpacity: 0.8,
                        shadowRadius: 2,
                        flexDirection:'row'}}>
                        <Text style= {{width:'100%',alignSelf:'center',textAlign:'center',fontSize:20,fontFamily:'Konnect-Medium',color:'white',padding:11}} >
                            UPDATE
                        </Text>

                        <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginLeft:-50,alignSelf:'center'}}
                               source={require('./right.png')}/>
                    </View>
                    </TouchableOpacity>



                    </KeyboardAwareScrollView>

                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,

        top: window.height/2,

        opacity: 0.5,

        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,

    },
})